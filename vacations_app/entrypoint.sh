#!/usr/bin/env bash

launch_gunicorn () {
  GUNICORN_CMD_ARGS_DEF="-b $HOST:$PORT -w $WORKERS -t $TIMEOUT --access-logfile '-' --worker-class aiohttp.GunicornUVLoopWebWorker $RELOAD"
  export GUNICORN_CMD_ARGS=${GUNICORN_CMD_ARGS:-$GUNICORN_CMD_ARGS_DEF}
  echo "Gunicorn config: '$GUNICORN_CMD_ARGS'"
  exec gunicorn app:app
}

set -e

cd $(dirname $(readlink -f $0))

case "$1" in

  prod)
    export RELOAD=""
    launch_gunicorn
  ;;

  local)
    WORKERS=1
    TIMEOUT=3600
    export RELOAD="--reload"
    launch_gunicorn
  ;;

  lint)
    echo "Linter problems found:"
    exec flake8
  ;;

  test)
    exec pytest --cov
  ;;

esac

exec "$@"