import sys
import signal
import asyncio

from api.main import create_app


def main_exit_handler(*args, **kwargs):
    # Avoid docker exit delay
    sys.exit(0)


signal.signal(signal.SIGTERM, main_exit_handler)

app = asyncio.run(create_app())
