

-- ACTIVITIES
CREATE TABLE activities(
    activities_id serial primary key,
    created_at timestamp default now(),
    name text not null,
    opening_hours jsonb,  -- I.e: {"mo": ["08:30-13:30", "16:00-18:00"], "tu": ["16:00-18:00"]}
    hours_spent float,
    category text,  -- I think I would add an array of categories instead.
    location text,  -- outdoors | indoors.
 -- It would be better a relation with another "territories" table, properly indexed, but by now:
    district text,
    latitude float not null,
    longitude float not null
);
SELECT cdb_cartodbfytable('juanra-carto', 'activities');
-- Re-do the stuff deleted by the cartodbfy process:
ALTER TABLE activities ADD COLUMN activities_id serial;
CREATE UNIQUE INDEX idx_activities_activities_id ON activities(activities_id);
ALTER TABLE activities ALTER COLUMN created_at SET DEFAULT now();

CREATE INDEX idx_activities_category ON activities(category);
CREATE INDEX idx_activities_location ON activities(location);
CREATE INDEX idx_activities_district ON activities(district);


-- After upload the file with data/import.py script:

INSERT INTO activities
    (opening_hours, hours_spent, category, location, district, latitude, longitude)
SELECT replace(opening_hours, '''', '"')::jsonb,
    hours_spent, category, location, district,
    ltrim(split_part(latlng, ',', 1), '[')::float as latitude,
    rtrim(split_part(latlng, ',', 2), ']')::float as longitude
FROM madrid_pd;

UPDATE activities SET the_geom = ST_SetSRID(ST_Point(longitude, latitude), 4326);


-- First I tried to import 'madrid.json' file to Carto via the dashboard, but the process un-quote the data
-- and delete the commas, so the 'opening_hours' field become an invalid and un-parseable json :(
-- After that I tried to run:

    --  INSERT INTO activities
    --      (opening_hours, hours_spent, category, location, district, latitude, longitude)
    --  SELECT opening_hours::jsonb, hours_spent, category, location, district,
    --      ltrim(split_part(latlng, ' ', 1), '[')::float as latitude,
    --      rtrim(split_part(latlng, ' ', 2), ']')::float as longitude
    --  FROM madrid;

-- So this method didn't work as expected.


