from cartoframes import CartoContext
import pandas as pd

madrid = pd.read_json('./madrid.json')
cc = CartoContext(
    base_url='https://<USER>.carto.com',
    api_key='<MASTER_API_KEY>'
)

cc.write(madrid, 'madrid_pd', overwrite=True)
