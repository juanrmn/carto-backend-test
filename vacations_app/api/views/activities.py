from aiohttp import web
from aiohttp_apispec import docs, request_schema, response_schema

from api.models.activities import ActivitiesModel
from api.utils.request import load_json

from api.schemas.commons import GeoJSONSchema
from api.schemas.activities import SearchActivitiesRequest, RecommendActivityRequest
from api.settings import config, logger as log  # noqa

routes = web.RouteTableDef()


@docs(tags=['activities'],
      summary='Get activity',
      description='Get activity details by ID')
@response_schema(GeoJSONSchema(strict=True), 200, description='Activity')
@routes.get('/{activity_id}')
async def get_activity(request):
    model = ActivitiesModel(request)
    activity_id = request.match_info['activity_id']

    data = await model.get_activity_by_id(activity_id)
    return web.json_response(data)


@docs(tags=['activities'],
      summary='Get activities',
      description='Search and get activities')
@request_schema(SearchActivitiesRequest(strict=True), location='body')
@response_schema(GeoJSONSchema(strict=True), 200, description='Activities')
@routes.post('/search')
async def search_activities(request):
    model = ActivitiesModel(request)
    params = await load_json(request)

    data = await model.get_activities(params)
    return web.json_response(data)


@docs(tags=['activities'],
      summary='Get recommended activity',
      description='Returns a recommended activity :)')
@request_schema(RecommendActivityRequest(strict=True), location='body')
@response_schema(GeoJSONSchema(strict=True), 200, description='Activities')
@routes.post('/recommend')
async def recommended_activity(request):
    model = ActivitiesModel(request)
    params = await load_json(request)

    # Aditionally accepts all the 'search' params to filter
    data = await model.get_recommended_activity(params)
    return web.json_response(data)
