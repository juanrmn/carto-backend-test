from aiohttp import web

from api.models.base import BaseModel
from api.settings import config, logger as log  # noqa


class ActivitiesModel(BaseModel):
    async def get_activity_by_id(self, activity_id):
        data = await self.carto.query(f'''
            SELECT row_to_json(fc) AS data
            FROM (
                SELECT
                    'Feature' AS "type",
                    ST_AsGeoJSON(activities.the_geom) :: json AS "geometry",
                    (
                        json_strip_nulls(row_to_json(activities))::jsonb
                            - 'cartodb_id' - 'the_geom' - 'the_geom_webmercator'
                    ) AS "properties"
                FROM activities
                WHERE activities.activities_id = {int(activity_id)}
            ) AS fc;
        ''')
        if len(data.rows) > 0:
            return data.rows[0]['data']

        raise web.HTTPNotFound

    async def get_activities(self, params):
        filters = self._get_filters(params)

        data = await self.carto.query(f'''
            SELECT row_to_json(fc) AS data
            FROM (
                SELECT
                    'FeatureCollection' AS "type",
                    array_to_json(array_agg(f)) AS "features"
                FROM (
                    SELECT
                        'Feature' AS "type",
                        ST_AsGeoJSON(the_geom) :: json AS "geometry",
                        (
                            json_strip_nulls(row_to_json(activities))::jsonb
                                - 'cartodb_id' - 'the_geom' - 'the_geom_webmercator'
                        ) AS "properties"
                    FROM activities
                    WHERE {filters}
                ) AS f
            ) AS fc;
        ''')

        if len(data.rows) > 0:
            return data.rows[0]['data']

        return {}

    async def get_recommended_activity(self, params):
        # Getting mandatory filters
        day_of_week = self._sanitize(params.get('time', {}).get('day_of_week'))
        time_range = self._sanitize(params.get('time', {}).get('time_range'))

        if not day_of_week or not time_range:
            raise web.HTTPNotFound

        filters = self._get_filters(params)
        data = await self.carto.query(f'''
            SELECT activities_id, category,	district, hours_spent, location,
                opening_hours->'{day_of_week}' as todays_opening_hours
            FROM activities
            WHERE {filters}
        ''')

        # The quey only filter by the day of the week, so let's check the time-ranges
        time_range = self._parse_time_range(time_range)
        choices = []
        for activity in data.rows:
            # Checks if the given 'time_range' fits the activity 'opening_hours'
            if self._fits_in_time_range(activity, time_range):
                choices.append(activity)

        if not choices:
            return self._empty_geojson()

        # If we got more than one, let's return the one with the longest 'hours_spent'
        choices = sorted(choices, key=lambda x: x['hours_spent'], reverse=True)
        activities_id = choices[0]['activities_id']
        return await self.get_activity_by_id(activities_id)

    def _fits_in_time_range(self, activity, time_range):
        minutes_spent = activity['hours_spent'] * 60  # To minutes...
        for opening_time_range in activity['todays_opening_hours']:
            activity_time_range = self._parse_time_range(opening_time_range)

            # checks if activity starts before the time_range
            #   and minutes_spent will fit into the time_range
            #   and minutes_spent will fit into the activity time (from user starting time)
            activity_fits = (activity_time_range[0] <= time_range[0]) \
                and (time_range[0] + minutes_spent) <= time_range[1] \
                and (time_range[0] + minutes_spent) <= activity_time_range[1]

            if activity_fits:
                return True

        return False

    def _parse_time_range(self, time_range_str):
        """ Returns a sorted array of integers that represents the
        minutes of the day for the given interval. I.e:
            ["01:00-01:30"] --> [60,90]
        """
        times_array = time_range_str.split('-')
        from_minutes = self._time_to_minutes(times_array[0])
        to_minutes = self._time_to_minutes(times_array[1])
        time_range = [from_minutes, to_minutes]

        return sorted(time_range)

    def _time_to_minutes(self, time_str):
        """ Receives a time in 24-hours format (i.e: 01:30), and
        returns the minute of the day (i.e: ==> 90)
        """
        time_array = time_str.split(':')
        hours = int(time_array[0])
        minutes = int(time_array[1])

        return (hours * 60) + minutes

    def _get_filters(self, params):
        filters = ['True']

        if params.get('category'):
            category = self._sanitize(params.get('category'))
            filters.append(
                f''' category ILIKE '{category}' '''
            )

        if params.get('location'):
            location = self._sanitize(params.get('location'))
            filters.append(
                f''' location ILIKE '{location}' '''
            )

        if params.get('district'):
            district = self._sanitize(params.get('district'))
            filters.append(
                f''' district ILIKE '{district}' '''
            )

        if params.get('day_of_week'):
            day_of_week = self._sanitize(params.get('day_of_week'))
            filters.append(
                f'''
                    opening_hours->'{day_of_week}' is not null
                    AND opening_hours->'{day_of_week}'->0 is not null
                '''
            )

        return ' AND '.join(filters)
