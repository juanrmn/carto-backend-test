from api.settings import config, logger as log  # noqa


class BaseModel:

    def __init__(self, app):
        # Shared DB connection
        self._app = app
        self.carto = app.config_dict['carto']

    def _sanitize(self, value, vtype=str):
        if not value:
            return ''
        if vtype is str:
            return value.replace("'", '')
        else:
            return vtype(value)

    def _empty_geojson(self):
        return {
            "type": "FeatureCollection",
            "features": None
        }
