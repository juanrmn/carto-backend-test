import aiohttp_cors
from aiohttp import web, ClientSession
from aiohttp_apispec import docs, response_schema
from longitude.core.data_sources.carto_async import CartoAsyncDataSource

from api.middlewares.base import setup_middlewares
from api.schemas.commons import EnvironmentSchema
from api.views.activities import routes as activities_routes
from api.settings import config, logger as log  # noqa


routes = web.RouteTableDef()

@docs(tags=['index'], summary='Index environment summary',
      description='API index with a little environment summary')
@response_schema(EnvironmentSchema(strict=True), 200,
                 description='Information about the running API')
@routes.get('/')
async def index(request):
    data = {
        'name': config.get('name'),
        'version': config.get('version'),
        'environment': config.get('environment'),
        'status': 'running'
    }
    return web.json_response(data)


async def create_app():
    """ Main entry point """
    # Setting app
    app = web.Application()
    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)

    # Public routes
    app.add_routes(routes)
    add_subapp(app, activities_routes, '/api/v1/activities')

    setup_middlewares(app)
    cors_setup(app)

    return app


async def on_startup(app):
    """ Called after application initialization, but before to server first request """
    app['carto_session'] = ClientSession()
    app['carto'] = CartoAsyncDataSource(
        user=config['carto']['user'],
        api_key=config['carto']['api_key'],
        options={'session': app['carto_session']}
    )


async def on_cleanup(app):
    """ Called just before shutdown """
    await app['carto_session'].close()


def add_subapp(app, routes, prefix, middlewares=[]):
    # Sharing objects
    subapp = web.Application()
    subapp.add_routes(routes)

    if middlewares:
        subapp.middlewares.extend(middlewares)

    # "Adding" subapp
    if not hasattr(app, 'subapps'):
        app['subapps'] = {}
    app['subapps'][prefix] = subapp

    # AIOHTTP's add
    app.add_subapp(prefix, subapp)


def cors_setup(app):
    cors = aiohttp_cors.setup(app, defaults={
        '*': aiohttp_cors.ResourceOptions(
            allow_methods='*',
            allow_credentials=True,
            allow_headers='*',
            expose_headers='*'
        )
    })
    for route in list(app.router.routes()):
        cors.add(route)
