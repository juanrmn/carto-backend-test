import logging
from environs import Env
from marshmallow.validate import OneOf


env = Env()


# Configuration dict
class Config(dict):

    ENVIRONMENTS = ['prod', 'staging', 'dev', 'local', 'test']

    def is_debug(self):
        return self.get('debug', False)


config = Config()
config['name'] = env.str('NAME', 'Vacations APP')
config['version'] = env.str('VERSION', '0.1.0')
config['environment'] = env.str(
    'ENVIRONMENT',
    'prod',
    validate=OneOf(
        config.ENVIRONMENTS,
        error='ENVIRONMENT must be one of: {choices}'
    )
)
config['debug'] = env.bool('DEBUG', False)

config['redis'] = {
    'host': env.str('REDIS_HOST', 'redis'),
    'port': env.int('REDIS_PORT', 6379),
    'password': env.str('REDIS_PASSWORD', 'redis_password')
}

config['carto'] = {
    'user': env.str('CARTO_USER', ''),
    'api_key': env.str('CARTO_API_KEY', '')
}


# Configuring logs
logging_level = logging.getLevelName('DEBUG' if config.get('debug') else 'INFO')
logging_format = '[%(asctime)s] (%(process)d) {%(filename)s:%(lineno)d} %(levelname)s - %(message)s'
loggin_datefmt = '%Y-%m-%dT%H:%M:%SZ'
logging.basicConfig(format=logging_format, datefmt=loggin_datefmt)

logger = logging.getLogger(config.get('log_name'))
logger.setLevel(logging_level)
