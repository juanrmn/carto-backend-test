import traceback

from aiohttp import web
from aiohttp_apispec import (
    AIOHTTP_APISpec, validate_request_middleware, validate_response_middleware
)
from api.settings import config, logger as log  # noqa


async def error_handler(exc, status=500):
    message = exc.args[0] if isinstance(exc.args[0], dict) else str(exc)

    data = {
        'message': message,
        'status_code': status
    }

    #  Calculate the traceback only in these cases
    if status >= 500 or config.is_debug():
        exc_traceback = traceback.format_exception(type(exc), exc, exc.__traceback__)
        exc_traceback = ''.join(exc_traceback)

        log.error(exc_traceback)

        if config.is_debug():
            data['exception'] = type(exc).__name__
            data['exception_traceback'] = exc_traceback

    return web.json_response(data, status=status)


@web.middleware
async def error_middleware(request, handler):
    try:
        return await handler(request)

    except Exception as exc:
        status = 500
        if hasattr(exc, 'status') and isinstance(exc.status, int):
            status = exc.status
        return await error_handler(exc, status)


def setup_middlewares(app):
    # Regular middlewares: error handler & APISpec / marshmallow validation
    app.middlewares.extend([
        error_middleware,
        validate_request_middleware,
        validate_response_middleware
    ])

    # APISpec
    AIOHTTP_APISpec(
        app=app,
        title=config.get('name'),
        version=config.get('version')
    )
