from marshmallow import Schema, fields
from api.settings import config


class EnvironmentSchema(Schema):
    version = fields.Str(required=True, default=config.get('version'), example='0.1.0')
    name = fields.Str(required=True, default=config.get('name'), example='API')
    environment = fields.Str(required=True, default=config.get('environment'), example='dev')
    status = fields.Str(example='ok')


class GeoJSONSchema(Schema):
    type = fields.Str(required=True, example='FeatureCollection')
    geometry = fields.Dict(example='{"type": "Point", "coordinates": [102.0, 0.5]}')
    feature = fields.List(
        fields.Dict, example='[{"type":"Feature","geometry":{"type":"Point","coordinates":[-5.9209,37.2829]}}]'
    )
