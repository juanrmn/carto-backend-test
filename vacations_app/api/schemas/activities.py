from marshmallow import Schema, fields, validate

LOCATIONS = ['outdoors', 'indoors']
DAYS_OF_WEEK = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']


class SearchActivitiesRequest(Schema):
    category = fields.Str(example='nature')
    district = fields.Str(example='Retiro')
    location = fields.Str(validate=validate.OneOf(choices=LOCATIONS))


class TimeSchema(Schema):
    day_of_week = fields.Str(required=True, validate=validate.OneOf(choices=DAYS_OF_WEEK))
    time_range = fields.Str(required=True, example='11:00-14:00')


class RecommendActivityRequest(Schema):
    category = fields.Str(example='cultural')
    time = fields.Nested(TimeSchema, required=True)
