# carto-backend-test

Link to the problem description: https://gist.github.com/alrocar/aff67c4352ffc663fad3eed1d8bdfea2

## Running the API:

First, clone this repository:

```bash
git clone git@bitbucket.org:juanrmn/carto-backend-test.git
```

    Note that credentials (with limited read-only access) are provided with the code (`vacations_app/.env`), along with basic settings.

### With docker-compose:

From the project root folder, simply run:

```bash
$ docker-compose up
```

### With a python virtual environment:

Also from the project root folder:

Setup python environment (Linux):

```bash
virtualenv -p python3.7 .venv
. .venv/bin/activate
pip install poetry
poetry install
```

Setup and running the project:

```bash
cd vacations_app
export $(grep -v '^#' .env | xargs -d '\n')

./entrypoint.sh local
```

## API docs:
http://localhost:8000/api_docs


----

### Notes

First, I've chosen the AioHttp framework because it's one of the last ones I've been working with, and I feel quite comfortable with it.

I've started from the source code of a previous project I was working on a few months ago. Back then, I spent a few hours deciding
how to organize and structure the code, and this is a simplification of the final structure.

The auto-documented API has several advantages, but it takes a bit longer than usual at the begining of a project. This is one of the reasons
why I've left out some important aspect of the code, like self-explaning errors returned to the API. It's quite important to let the user
know when the introduced time-range is not right, for example. But I didn't want to spend much more time than the required for the test.
So, this would go to the TODO list :)

#### About the time-range tradeoff

I've left in the python side the function to decide if a given activity fits the vacation-goer time-range. The BD query filters only by the day
of the week (not too hard for a jsonb column), but the time-ranges checking could be quite triky, to do it in SQL, I think.
However, it seems to me more readable and maintainable this way (I would preffer this over the performance at this stage of the project).

Perhaps, if the number of activities increases enough, it would be better to pre-format the time ranges in a better way so that we can filter the
list right from the DB, but in my opinion it should be at the time when the performance become a critical aspect)

#### Comments about the future
* Do not recommend an outdoors activity on a rainy day

Right now, the `location` could be filterd by the user, but it would be nice to integrate with a public Weather service (like openweathermap.org)
and get the current weather at the activity places to take it into account (also we may need some changes to the model to add the City name).

* Support getting information about activities in multiple cities

I think it would only take a few changes to each part of the APP, for example to the model (both the DB and API models) to include the new
Cities relations and fields, and also the Views and Schemas to include it in the API params.

* Extend the recommendation API to fill the given time range with multiple activities

This function it's quite more difficult to integrate indeed. First, there could be many possible solutions to a given situation, and we'll have
to choose one of them, and of course may not be the optimal one.

Then there is the thing about the travel time between the activities. I've been playing with the Google OR-Tools a few months ago about to solve
the travelling salesman problem, and it's the same case, I think. So it isn't impossible, but it's not trivial.
